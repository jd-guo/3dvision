# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/jd/git/3dvision/src/realdata.cpp" "/home/jd/git/3dvision/src/CMakeFiles/RealData.dir/realdata.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "CGAL_USE_GMP"
  "CGAL_USE_MPFR"
  "CGAL_USE_ZLIB"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/jd/git/3dvision/lib/depth_map/CMakeFiles/depthMapLib.dir/DependInfo.cmake"
  "/home/jd/git/3dvision/lib/ray_structure/CMakeFiles/rayStructureLib.dir/DependInfo.cmake"
  "/home/jd/git/3dvision/lib/max_flow/CMakeFiles/maxFlowLib.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/x86_64-linux-gnu"
  "."
  "/usr/local/include"
  "/usr/include/opencv"
  "/usr/include/eigen3"
  "src/PSL"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
