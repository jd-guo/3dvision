#include "../lib/max_flow/optimizer.h"
#include "../lib/ray_structure/typedef.h"
#include "../lib/ray_structure/raystructure.h"
#include "../lib/ray_structure/vertex.h"
#include "../lib/eva_gt/eva_gt.h"
#include <iostream>
#include <string>
#include <cassert>
#include <array>
#include <math.h>
#include <ctime>


int main() {

    // set for running time
    time_t tstart, tend;
    tstart = time(0);

    // Select one of the four methods
    const bool OUR_METHOD = false;
    const bool VARIABLE_LAMBDA = false;

    // set parameters
    const int NUM_DEPTHMAPS = 11;
    const int STEP_SAMPLING = 150;
    const int NUM_ITER = 5;

    int lambda; // only matters if variable lambda set to be false
    int alpha;

    const int WINDOW_SIZE = 2;
    std::vector<double> cooefficient;
    if (OUR_METHOD){
        lambda =58;  // Best for unvariable lambda:58
        alpha=150;
    }
    else {
        alpha=3; // Best for unvariable lambda:3/1
        lambda=0;
    }


    // set i/o path
    std::string folder_depthmap, folder_bbox, folder_results, ground_truth_file_name;
    folder_depthmap = "../../data/depthMaps/";
    folder_bbox     = "../../data/boundingBox/fountain_dense/urd/";
    folder_results  = "../../results/";
    ground_truth_file_name = "../../data/fountain.ply";


    // load depth maps
    PSL::DepthMap<float,double> tmpMaps;
    std::vector<PSL::DepthMap<float,double> > depthMaps = RS::load_depthmaps(folder_depthmap,tmpMaps,NUM_DEPTHMAPS);


    // get 3D points within bounding box
    std::vector<RS::Vertex> pcd_original = RS::get_pointcloud(depthMaps,NUM_DEPTHMAPS,folder_bbox);
    std::cout << "3D point cloud loaded, "
              << "Total number of Points: " << pcd_original.size() << std::endl;
/*
    // save pcd
    std::cout << "Saving original point cloud ... " << std::endl;
    RS::save_pointcloud(pcd_original,folder_results,"pcd_origin10000al_100",100);
*/
    std::cout << "Done" << std::endl << std::endl;


    // sample point cloud
    std::vector<RS::Vertex> pcd_sampled = RS::sample_pointcloud(pcd_original,STEP_SAMPLING);
    std::cout << "Downsampling every " << STEP_SAMPLING <<  " points, "
              << "Reduced number of Points: " << pcd_sampled.size() << std::endl;
/*
    // save downsampled pcd
    std::cout << "Saving downsampled point cloud ... " << std::endl;
    RS::save_pointcloud(pcd_sampled,folder_results,"pcd_sampled",1);
*/
    std::cout << "Done" << std::endl <<std::endl;


    // set index of each vertex
    std::cout << "Setting vertex index ..." << std::endl;
    std::vector<RS::Vertex> pcd = pcd_sampled;
    RS::assign_vertex_index(pcd);
    std::vector<std::pair<Point,Eigen::Vector2i> > vertex;
    for (int i=0; i<pcd.size(); i++) {
        vertex.push_back( pcd[i].get_pair());
    }
    const int NUM_VERTICES = vertex.size();
    std::cout << "Done" << std::endl << std::endl;


    // generate Delaunay triangulation
    std::cout << "Generating triangulaton ..." << std::endl;
    Delaunay T(vertex.begin(),vertex.end());
    // verify triangulation
    assert(T.is_valid());
    assert(T.dimension() == 3);
    std::cout << "Number of vertices: " << T.number_of_vertices() << std::endl;
    std::cout << "Number of cells: " << T.number_of_cells() << std::endl;
    std::cout << "Done" << std::endl << std::endl;


    // set all cell indices to -1
    std::cout << "Setting indices of all cells to -1 ..." << std::endl;
    const int NUM_ALL_CELLS = RS::assign_all_cell_index(T);
    std::cout << "Number of all the indexed cells: " << NUM_ALL_CELLS << std::endl;
    std::cout << "Done" << std::endl << std::endl;


    // construct database, set indices to finite cells
    std::cout << "Setting indices of finite cells ... " << std::endl;
    const int NUM_FINITE_CELLS = RS::assign_finite_cell_index(T);
    std::cout << "Number of indexed finite cells: " << NUM_FINITE_CELLS << std::endl;
    std::cout << "Done" << std::endl << std::endl;


    // get vertex indices of cell
    std::cout << "Getting 4 vertex indices of every cell ..." << std::endl;
    Eigen::MatrixXi index_vertex_per_cell = RS::get_vertex_index_per_cell(T);
    std::cout << "Size of vertex indices of every cell: " << index_vertex_per_cell.rows() << " x " << index_vertex_per_cell.cols() << std::endl;
/*
    // save vertex indices of cell
    std::cout << "Saving vertex indices of every cell" << std::endl;
    RS::save_index_vertex_per_cell(index_vertex_per_cell, folder_results, "index_vertex_per_cell");
*/
    std::cout << "Done" << std::endl << std::endl;


    // get vertex coordinates
    std::cout << "Getting coordinates of every vertex ..." << std::endl;
    Eigen::MatrixXd coord_vertex = RS::get_vertex_coord(T);
    std::cout << "Size of Vertex coordinates: " << coord_vertex.rows() << " x " << coord_vertex.cols() << std::endl;
/*
    // save vetex coordinates
    std::cout << "Saving vertex coordinates" << std::endl;
    RS::save_coord_vertex(coord_vertex, folder_results, "coord_vetex");
*/
    std::cout << "Done" << std::endl << std::endl;

/*
    // generate mesh of Delaunay triangulation
    std::vector<std::array< int,3>> surface_triangulation = RS::get_surface_triangulation(T);
    RS::writeOBJ(folder_results,"mesh_triangulation",coord_vertex,surface_triangulation);
*/


    // --------- ray structure --------- //
    // get ray_traversals_ and ray_depths_
    std::cout << "Building up ray structure ..." << std::endl;
    std::pair< std::vector<std::vector<int>>,std::vector<int> > index_along_ray = RS::traverse_ray(T,depthMaps,WINDOW_SIZE);
    std::vector<std::vector<int>> ray_traversals_ = index_along_ray.first;
    std::vector<int> ray_depths_ = index_along_ray.second;
    std::cout << "Number of ray traversals: " << ray_traversals_.size() << std::endl;
    std::cout << "Number of ray_depths: " << ray_depths_.size() << std::endl;

/*
    // save ray indices
    std::cout << "Saving ray traversals and ray depths" << std::endl;
    RS::save_ray_traversals_(ray_traversals_,folder_results,"ray_traversals");
    RS::save_ray_depths_(ray_depths_,folder_results,"ray_depths");
*/
    std::cout << "Done" << std::endl<<std::endl;


    // get neighbor indices
    std::cout << "Calculating Neighborhood Information ..." << std::endl; ;
    std::vector<std::array<int,4>> neighbors_ = RS::get_neighbors_(T);
    std::vector<std::array<double,4>> neighbors_surface_ = RS::get_neighbors_surface_(T);
    std::cout << "Number of neighbors_: " << neighbors_.size() << std::endl;

/*
    // save neighbor indices
    std::cout << "Saving neighbors_" << std::endl;
    RS::save_neighbors_surface_(neighbors_surface_,folder_results,"neighbors");
*/
    std::cout << "Done" << std::endl << std::endl;


    // write README.txt
//    RS::write_readme(NUM_FINITE_CELLS, NUM_RAYS, NUM_VERTICES,folder_results);


    // --------- GRAPH CUT --------- //

    if (VARIABLE_LAMBDA){
        std::cout << "Optimizing with alpha:" << alpha <<" and variable lambda" <<std::endl;
    }
    else {
        std::cout << "Optimizing with alpha: " << alpha << ", lambda: " << lambda << std::endl;
    }

    std::vector<int> labels;
    std::vector<int> index_cell_on_bbox;

    if (OUR_METHOD) {
        std::cout << "Using Our method, ";
        labels = GC::get_labels(ray_depths_, ray_traversals_, neighbors_,neighbors_surface_,cooefficient,
                                alpha, lambda, NUM_FINITE_CELLS, NUM_ITER,VARIABLE_LAMBDA,index_cell_on_bbox,WINDOW_SIZE);

    }
    else {
        std::cout << "Using Vu's method, ";
        labels = GC::get_labels_vu(ray_depths_, ray_traversals_, neighbors_,neighbors_surface_,
                                   alpha, lambda, NUM_FINITE_CELLS,VARIABLE_LAMBDA,index_cell_on_bbox);
    }

    std::cout << "Graph Cut is Done" << std::endl << std::endl;
    // total running time
    tend = time(0);
    std::cout << "Running time: "<< difftime(tend, tstart) << std::endl;


    // generate mesh
    std::cout << "Extracting surfaces ..." << std::endl;
    std::vector<std::array<int,3>> surface = RS::get_surface(T,labels);
    std::cout << "The surface consists of " << surface.size() << " triangles " << std::endl;

    if (surface.size()!=0) {
        // save mesh
        std::cout << "Saving mesh ..." << std::endl;
        std::string filename_mesh;

        if (VARIABLE_LAMBDA) {
            if (OUR_METHOD) {
                filename_mesh = "mesh_" + std::to_string(lround(alpha)) + "_varLambda_" +std::to_string(STEP_SAMPLING) + "_Ours";}
            else {
                filename_mesh = "mesh_" + std::to_string(lround(alpha)) + "_varLambda_" +std::to_string(STEP_SAMPLING) + "_Vu";
            }
        }
        else {
            if (OUR_METHOD) {
                filename_mesh = "mesh_" + std::to_string(lround(alpha)) + "_" + std::to_string(lambda) + "_" +std::to_string(STEP_SAMPLING) + "_Ours";}
            else {
                filename_mesh = "mesh_" + std::to_string(lround(alpha)) + "_" + std::to_string(lambda) + "_" +std::to_string(STEP_SAMPLING) + "_Vu";
            }
        }

        RS::writeOBJ(folder_results,filename_mesh,coord_vertex,surface);
        std::cout << "Done" << std::endl << std::endl;

        // Evaluation
        std::cout << "Comparing with ground truth ..." << std::endl;
        std::string result_file_name = folder_results + filename_mesh +".obj";

        EV::save_reconstruction_unsigned_errors(ground_truth_file_name, result_file_name);
        std::cout << "Done" << std::endl << std::endl << std::endl;
    }

    else {
        std::cout << "No surface" << std::endl << std::endl << std::endl;
    }

    return 0;
}
