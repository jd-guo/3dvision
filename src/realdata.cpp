#include "../lib/max_flow/optimizer.h"
#include "../lib/ray_structure/typedef.h"
#include "../lib/ray_structure/raystructure.h"
#include "../lib/ray_structure/vertex.h"
#include "../lib/eva_gt/eva_gt.h"
#include <iostream>
#include <string>
#include <cassert>
#include <array>
#include <math.h>
#include <ctime>



int main() {

    // set for running time
    time_t tstart, tend;
    tstart = time(0);

    // Select one of the four methods
//    const bool OUR_METHOD = false;
    int STEP_SAMPLING;
    std::cout << "Type in the Sampling ratio from the original Point Cloud:\n";
    std::cin >> STEP_SAMPLING;

    std::cout << "Do you wish to use precomputed ray structure with 1 per cent sampling? type 1 for yes.\n ";
    int precomputed=0;
    std::cin >> precomputed;

    bool VARIABLE_LAMBDA = false;
    std::cout << "Choose whether variable lambda should be used, 1 for yes: \n";
    std::cin >> VARIABLE_LAMBDA;

    // set parameters
    const int NUM_DEPTHMAPS = 127;
    const int NUM_ITER = 5;
    std::vector<double> cooefficient;
//    int lambda; // only matters if variable lambda set to be false
//    int alpha;
    int lambda;
    std::cout << "Type in the Window size should be used: \n";
    int WINDOW_SIZE;
    std::cin >> WINDOW_SIZE;
    std::cout << "Type in the cooefficients should be used along the entire window, the middle one is 1 and starts from behind:\n";

    for (int i = 0; i < (2*WINDOW_SIZE+1); i++){
      double cooef;
      std::cin >> cooef;
      cooefficient.push_back(cooef);
    }
    std::cout << "Type in the cooefficient for the regularization term lambda: \n";
    std::cin >> lambda;
   // doesn't matter if variable
    //if (OUR_METHOD){
        //const int lambda_our =50;  // Best for unvariable lambda:58
        const int alpha_our=150;
    //}
    //else {
        const int alpha_Vu=200; // Best for unvariable lambda:3/1
        //lambda=5;
    //}


    // set i/o path
    std::string folder_depthmap, folder_results, folder_bbox;
    folder_depthmap = "../data/depthMaps_thesis_after_cvpr16_submission/";
    folder_results  = "../results/real_data/";
    folder_bbox     = "../data/depthMaps_thesis_after_cvpr16_submission/boundingBox/";

    // All neccessary data structure
    std::vector<std::array<int,4>> neighbors_;
    std::vector<std::array<double,4>> neighbors_surface_;
    std::vector<std::vector<int>> ray_traversals_ ;
    std::vector<int> ray_depths_ ;
    std::vector<int> index_cell_on_bbox;
    Eigen::MatrixXd coord_vertex;
    std::vector<std::pair<Point,Eigen::Vector2i> > vertex;
    Delaunay T;
    int NUM_FINITE_CELLS;

    if (precomputed == 0){

      // load depth maps
      PSL::DepthMap<float,double> tmpMaps;
      std::vector<PSL::DepthMap<float,double> > depthMaps = RS::load_depthmaps(folder_depthmap,tmpMaps,NUM_DEPTHMAPS);

      // get 3D points within bounding box
      std::vector<RS::Vertex> pcd_original = RS::get_pointcloud(depthMaps,NUM_DEPTHMAPS,folder_bbox);
      std::cout << "3D point cloud loaded, "
                << "Total number of Points: " << pcd_original.size() << std::endl;
  /*
      // save pcd
      std::cout << "Saving original point cloud ... " << std::endl;
      RS::save_pointcloud(pcd_original,folder_results,"pcd_origin10000al_100",100);
  */
      std::cout << "Done" << std::endl << std::endl;


      // sample point cloud
      std::vector<RS::Vertex> pcd_sampled = RS::sample_pointcloud(pcd_original,STEP_SAMPLING);
      std::cout << "Downsampling every " << STEP_SAMPLING <<  " points, "
                << "Reduced number of Points: " << pcd_sampled.size() << std::endl;


      // add bounding box
      std::string path_bbox = folder_bbox + "0000.png.bounding";
      Eigen::Matrix<double,2,3> bbox = RS::load_bbox(path_bbox);
      for (int i=0; i<2; ++i) {
          for (int j=0; j<2; ++j) {
              for (int k=0; k<2; ++k) {
                  double x, y, z;
                  x = bbox(i,0);
                  y = bbox(j,1);
                  z = bbox(k,2);
                  pcd_sampled.push_back(RS::Vertex(Point(x,y,z),-1));
              }
          }
      }


      // save downsampled pcd
      //std::cout << "Saving downsampled point cloud ... " << std::endl;
      //RS::save_pointcloud(pcd_sampled,folder_results,"pcd_sampled",1);
      //std::cout << "Done" << std::endl <<std::endl;


      // set index of each vertex
      std::cout << "Setting vertex index ..." << std::endl;
      std::vector<RS::Vertex> pcd = pcd_sampled;
      RS::assign_vertex_index(pcd);
      for (int i=0; i<pcd.size(); i++) {
          vertex.push_back( pcd[i].get_pair());
      }
      const int NUM_VERTICES = vertex.size();
      std::cout << "Done" << std::endl << std::endl;

      std::cout << "Saving vertices for reconstructing the Delaunay triangulation...";
      RS::save_delaunay_data(vertex,folder_results,"delaunay_data");

      // generate Delaunay triangulation
      std::cout << "Generating triangulaton ..." << std::endl;
      T.insert(vertex.begin(),vertex.end());
      // verify triangulation
      assert(T.is_valid());
      assert(T.dimension() == 3);
      std::cout << "Number of vertices: " << T.number_of_vertices() << std::endl;
      std::cout << "Number of cells: " << T.number_of_cells() << std::endl;
      std::cout << "Done" << std::endl << std::endl;


      // set all cell indices to -1
      std::cout << "Setting indices of all cells to -1 ..." << std::endl;
      const int NUM_ALL_CELLS = RS::assign_all_cell_index(T);
      std::cout << "Number of all the indexed cells: " << NUM_ALL_CELLS << std::endl;
      std::cout << "Done" << std::endl << std::endl;


      // construct database, set indices to finite cells
      std::cout << "Setting indices of finite cells ... " << std::endl;
      NUM_FINITE_CELLS = RS::assign_finite_cell_index(T);
      std::cout << "Number of indexed finite cells: " << NUM_FINITE_CELLS << std::endl;
      std::cout << "Done" << std::endl << std::endl;


      // get vertex indices of cell
      std::cout << "Getting 4 vertex indices of every cell ..." << std::endl;
      Eigen::MatrixXi index_vertex_per_cell = RS::get_vertex_index_per_cell(T);
      std::cout << "Size of vertex indices of every cell: " << index_vertex_per_cell.rows() << " x " << index_vertex_per_cell.cols() << std::endl;

      // save vertex indices of cell
      std::cout << "Saving vertex indices of every cell" << std::endl;
      RS::save_index_vertex_per_cell(index_vertex_per_cell, folder_results, "index_vertex_per_cell");

      std::cout << "Done" << std::endl << std::endl;


      // get vertex coordinates
      std::cout << "Getting coordinates of every vertex ..." << std::endl;
      coord_vertex = RS::get_vertex_coord(T);
      std::cout << "Size of Vertex coordinates: " << coord_vertex.rows() << " x " << coord_vertex.cols() << std::endl;



     // save vetex coordinates
      std::cout << "Saving vertex coordinates" << std::endl;
      RS::save_coord_vertex(coord_vertex, folder_results, "coord_vertex");
      std::cout << "Done" << std::endl << std::endl;

      // get cell index on bbox
      std::cout << "Getting cells on the bounding box" << std::endl;

      index_cell_on_bbox = RS::get_index_on_bbox(coord_vertex, bbox, index_vertex_per_cell);
      RS::save_cell_on_bbox(index_cell_on_bbox, folder_results, "cell_on_bbox");


      // generate mesh of Delaunay triangulation
  //    std::vector<std::array< int,3>> surface_triangulation = RS::get_surface_triangulation(T);
  //    RS::writeOBJ(folder_results,"mesh_triangulation",coord_vertex,surface_triangulation);



      // --------- ray structure --------- //
      // get ray_traversals_ and ray_depths_
      std::cout << "Building up ray structure ..." << std::endl;
      std::pair< std::vector<std::vector<int>>,std::vector<int> > index_along_ray = RS::traverse_ray(T,depthMaps,WINDOW_SIZE);
      ray_traversals_ = index_along_ray.first;
      ray_depths_ = index_along_ray.second;
      std::cout << "Number of ray traversals: " << ray_traversals_.size() << std::endl;
      std::cout << "Number of ray_depths: " << ray_depths_.size() << std::endl;


      // save ray indices
      std::cout << "Saving ray traversals and ray depths" << std::endl;
      RS::save_ray_traversals_(ray_traversals_,folder_results,"ray_traversals");
      RS::save_ray_depths_(ray_depths_,folder_results,"ray_depths");

      std::cout << "Done" << std::endl<<std::endl;


      // get neighbor indices
      std::cout << "Calculating Neighborhood Information ..." << std::endl; ;
      neighbors_ = RS::get_neighbors_(T);
      neighbors_surface_ = RS::get_neighbors_surface_(T);
      std::cout << "Number of neighbors_: " << neighbors_.size() << std::endl;


      // save neighbor indices
      std::cout << "Saving neighbors_" << std::endl;
      RS::save_neighbors_surface_(neighbors_surface_,folder_results,"neighbors_surface_");
      RS::save_neighbors_(neighbors_,folder_results,"neighbors");
      std::cout << "Done" << std::endl << std::endl;


      // write README.txt
  //    RS::write_readme(NUM_FINITE_CELLS, NUM_RAYS, NUM_VERTICES,folder_results);
    }
    else {
    // --------- GRAPH CUT --------- //
    // Loading existing ray indices
    std::cout << "Loading all neccessary data for the graph cut algorithm...\n";
    neighbors_=RS::load_neighbors_(folder_results,"neighbors");
    std::cout << "Loaded neighbor Information for " << neighbors_.size() << " cells. \n";
    neighbors_surface_=RS::load_neighbors_surface_(folder_results,"neighbors_surface_");
    std::cout <<  "Loaded neighbor surface Information for " << neighbors_surface_.size() << " cells. \n";
    ray_traversals_ = RS::load_ray_traversals_(folder_results,"ray_traversals");
    std::cout <<  "Loaded Ray traversal Information for " << ray_traversals_.size() << " rays. \n";
    ray_depths_ = RS::load_ray_depths_(folder_results,"ray_depths");
    std::cout <<  "Loaded ray depth Information for " << ray_depths_.size() << " ray depths. \n";
    index_cell_on_bbox = RS::load_cell_on_bbox(folder_results,"cell_on_bbox");
    std::cout <<  "Loaded bbox boundary cells " << index_cell_on_bbox.size() << " cells. \n";
    coord_vertex = RS::load_coord_vertex(folder_results,"coord_vertex");
    std::cout <<  "Loaded Vertex cooridnates " << coord_vertex.size() << " vertices. \n";
    vertex = RS::load_delaunay_data(folder_results,"delaunay_data");
    std::cout << "Loaded vertices for Delaunay" << vertex.size() << " vertices. \n";
    NUM_FINITE_CELLS  = neighbors_.size();
    std::cout << "Generating triangulaton ..." << std::endl;
    T.insert(vertex.begin(),vertex.end());
    // verify triangulation
    assert(T.is_valid());
    assert(T.dimension() == 3);
    }
//    if (VARIABLE_LAMBDA){
//        std::cout << "Optimizing with alpha:" << alpha <<" and variable lambda" <<std::endl;
//    }
//    else {
//        std::cout << "Optimizing with alpha: " << alpha << ", lambda: " << lambda << std::endl;
//    }

//    labels;
//    if (OUR_METHOD) {
    std::cout << "Using Our method, ";
    std::vector<int> labels_our = GC::get_labels(ray_depths_, ray_traversals_, neighbors_,neighbors_surface_,cooefficient,
                                alpha_our, lambda, NUM_FINITE_CELLS, NUM_ITER,VARIABLE_LAMBDA,index_cell_on_bbox,WINDOW_SIZE);

//    }
//    else {
        //std::cout << "Using Vu's method, currently skipped";
        //std::vector<int> labels_Vu = GC::get_labels_vu(ray_depths_, ray_traversals_, neighbors_,neighbors_surface_,
        //                           alpha_Vu, lambda, NUM_FINITE_CELLS, VARIABLE_LAMBDA,index_cell_on_bbox);
//    }

    std::cout << "Graph Cut is Done" << std::endl << std::endl;
    // total running time
    tend = time(0);
    std::cout << "Running time: "<< difftime(tend, tstart) << std::endl;


    // generate mesh
    std::cout << "Extracting surfaces ..." << std::endl;
    std::vector<std::array<int,3>> surface_our = RS::get_surface(T,labels_our);
    std::cout << "The surface consists of " << surface_our.size() << " triangles " << std::endl;

    //std::cout << "Extracting surfaces ..." << std::endl;
    //std::vector<std::array<int,3>> surface_Vu = RS::get_surface(T,labels_Vu);
    //std::cout << "The surface consists of " << surface_Vu.size() << " triangles " << std::endl;

    if (surface_our.size()!=0) {
        // save mesh
        std::cout << "Saving mesh ..." << std::endl;
        //std::string filename_mesh;

//        if (VARIABLE_LAMBDA) {
//            if (OUR_METHOD) {
              //  std::string filename_our_mesh = "mesh_" + std::to_string(lround(alpha_our)) + "_varLambda_" +std::to_string(STEP_SAMPLING) + "_Ours";
//    }
//            else {
              //  std::string filename_Vu_mesh = "mesh_" + std::to_string(lround(alpha_Vu)) + "_varLambda_" +std::to_string(STEP_SAMPLING) + "_Vu";
//            }
//        }
//        else {
//            if (OUR_METHOD) {
                std::string filename_our_mesh  = "mesh_" + std::to_string(lround(alpha_our)) + "_" + std::to_string(lambda) + "_" +std::to_string(STEP_SAMPLING) + "_Ours";
//              }
//            else {
//                filename_mesh = "mesh_" + std::to_string(lround(alpha)) + "_" + std::to_string(lambda) + "_" +std::to_string(STEP_SAMPLING) + "_Vu";
//            }
//        }

        RS::writeOBJ(folder_results,filename_our_mesh,coord_vertex,surface_our);
    //    RS::writeOBJ(folder_results,filename_Vu_mesh,coord_vertex,surface_Vu);
        std::cout << "Done" << std::endl << std::endl;
    }

    else {
        std::cout << "No surface" << std::endl << std::endl << std::endl;
    }

    return 0;

}
