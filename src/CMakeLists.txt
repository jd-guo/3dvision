#add_executable(3DVisiontest "test.cpp")
#add_subdirectory(PSL)
#target_link_libraries(3Dvisiontest depthMapLib ${OpenCV_LIBS} ${BOOST_LIBS})

#include_directories("include")
#add_subdirectory(RayStructure)


add_executable(Raystructure "main.cpp")
add_executable(RealData "realdata.cpp")

# CGAL
target_link_libraries(Raystructure
  ${CGAL_LIBRARY} )

# Boost
target_link_libraries(Raystructure
  ${Boost_FILESYSTEM_LIBRARY}
  ${Boost_SYSTEM_LIBRARY}
  ${Boost_PROGRAM_OPTIONS_LIBRARY})

# OpenCV
target_link_libraries(Raystructure
  ${OpenCV_LIBRARIES})

# DepthMap
target_link_libraries(Raystructure
  depthMapLib)

# MaxFlow
target_link_libraries(Raystructure
  maxFlowLib)

# RayStructure
target_link_libraries(Raystructure
  rayStructureLib)

# Evaluation
target_link_libraries(Raystructure
   "/usr/local/lib/libflann.so")
target_link_libraries(Raystructure
   evaGtLib)

target_link_libraries(RealData
  ${CGAL_LIBRARY}
  ${Boost_FILESYSTEM_LIBRARY}
  ${Boost_SYSTEM_LIBRARY}
  ${Boost_PROGRAM_OPTIONS_LIBRARY}
  ${OpenCV_LIBRARIES}
  depthMapLib
  rayStructureLib
  maxFlowLib)
