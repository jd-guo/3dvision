#ifndef EVA_GT_H
#define EVA_GT_H
#include <flann/flann.hpp>

namespace EV
{
    void save_reconstruction_unsigned_errors(const std::string& ground_truth_file_name,const std::string& result_file_name);
}

#endif // EVA_GT_H
