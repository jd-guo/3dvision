#include "vertex.h"
#include <utility>

namespace RS
{
    Vertex::Vertex() {
        point = Point(0,0,0);
        index_cam = -1;
        index_vertex = -1;
    }

    Vertex::Vertex(Point pt, int idx_cam) {
        point = pt;
        index_cam = idx_cam;
        index_vertex = -1;
    }

    void Vertex::set_point(Point pt) {
        point = pt;
    }

    void Vertex::set_index_cam(int idx) {
        index_cam = idx;
    }

    void Vertex::set_index_vertex(int idx) {
        index_vertex = idx;
    }

    std::pair<Point, Eigen::Vector2i> Vertex::get_pair() {
        return std::make_pair(get_point(),get_index());
    }

    Point Vertex::get_point() {
        return point;
    }

    double Vertex::get_point(int i) {
        return point[i];
    }

    Eigen::Vector2i Vertex::get_index() {
        return Eigen::Vector2i(get_index_cam(),get_index_vertex());
    }

    int Vertex::get_index_cam() {
        return index_cam;
    }

    int Vertex::get_index_vertex() {
        return index_vertex;
    }
}
