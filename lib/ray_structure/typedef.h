#ifndef TYPEDEF_H
#define TYPEDEF_H

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Triangulation_data_structure_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/Triangulation_cell_base_with_info_3.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <Eigen/Dense>
#include "../max_flow/optimizer.h"


// CGAL Kernel
typedef CGAL::Exact_predicates_inexact_constructions_kernel                     Kernel;

// General primitives
typedef Kernel::Ray_3                                                           Ray;
typedef Kernel::Triangle_3                                                      Triangle;
typedef Kernel::Segment_3                                                       Segment;

// Delaunay triangulation
typedef CGAL::Triangulation_vertex_base_with_info_3<Eigen::Vector2i, Kernel>    Vb;
typedef CGAL::Triangulation_cell_base_with_info_3<unsigned int, Kernel>         Cb;
typedef CGAL::Triangulation_data_structure_3<Vb,Cb>                             Tds;
typedef CGAL::Delaunay_triangulation_3<Kernel, Tds>                             Delaunay;

// Delaunay primitives
typedef Delaunay::Point                                                         Point;
typedef Delaunay::Cell_handle                                                   Cell_handle;
typedef Delaunay::Facet                                                         Facet;
typedef Delaunay::Finite_cells_iterator                                         Cells_iterator;
typedef Delaunay::All_cells_iterator                                            All_cells_iterator;
typedef Delaunay::Finite_vertices_iterator                                      Vertices_iterator;
typedef Delaunay::Finite_facets_iterator                                        Facets_iterator;

// Graph
typedef Graph<int,int,int> GraphType;



#endif // TYPEDEF_H
