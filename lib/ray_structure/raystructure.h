#ifndef RAYSTRUCTURE_H
#define RAYSTRUCTURE_H

#include <psl_base/depthMap.h>
#include <Eigen/Dense>
#include <iterator>
#include <array>
#include "vertex.h"
#include <iostream>
#include <utility>

namespace RS
{
    std::vector<std::array< int,3>> get_surface_triangulation(const Delaunay &T);

    const std::vector<PSL::DepthMap<float,double> > load_depthmaps(const std::string& folder_depthmap, PSL::DepthMap<float,double> tmpMap, const int NUM_DEPTHMAPS);

    const std::vector<Vertex> get_pointcloud(const std::vector< PSL::DepthMap<float,double> > depthMaps, const int NUM_DEPTHMAPS, const std::string& folder_bbox);
    const std::vector<Vertex> sample_pointcloud(std::vector<Vertex> pcd_original, int STEP_SAMPLING);

    const Eigen::Matrix<double,2,3> load_bbox(const std::string& path);
    const std::vector< std::array<int, 4> > get_neighbors_(Delaunay &T);
    const std::vector< std::array<double, 4> > get_neighbors_surface_(Delaunay &T);

    void save_pointcloud(std::vector<Vertex> &pcd, const std::string& folder, const std::string& filename, const int step);
    void save_index_vertex_per_cell(Eigen::MatrixXi &index, const std::string& folder, const std::string& filename);
    void save_coord_vertex(Eigen::MatrixXd &coord, const std::string& folder, const std::string& filename);
    void save_ray_traversals_(std::vector<std::vector<int> > &ray_traversals_, const std::string& folder, const std::string& filename);
    void save_ray_depths_(std::vector<int> &ray_depths_, const std::string& folder, const std::string& filename);
    void save_neighbors_(std::vector<std::array<int,4>> &neighbors_, const std::string& folder, const std::string& filename);
    void save_cell_on_bbox(std::vector<int> index_cell_on_bbox, const std::string& folder, const std::string& filename);
    void save_delaunay_data(std::vector<std::pair<Point,Eigen::Vector2i> > vertex,const std::string& folder, const std::string& filename);
    std::vector<std::array<int,4>> load_neighbors_(const std::string& folder, const std::string& filename);
    std::vector<std::array<double,4>> load_neighbors_surface_(const std::string& folder, const std::string& filename);
    std::vector<std::vector<int>> load_ray_traversals_(const std::string& folder, const std::string& filename);
    std::vector<int> load_ray_depths_(const std::string& folder, const std::string& filename);
    std::vector<int> load_cell_on_bbox(const std::string& folder, const std::string& filename);
    Eigen::MatrixXd load_coord_vertex(const std::string& folder, const std::string& filename);
    std::vector<std::pair<Point,Eigen::Vector2i> > load_delaunay_data(const std::string& folder, const std::string& filename);


    void assign_vertex_index(std::vector<Vertex> &pcd);
    void write_readme(const int NUM_FINITE_CELLS, const int NUM_RAYS, const int NUM_VERTICES, const std::string& folder);
    void writeOBJ(const std::string &folder, const std::string &filename, const Eigen::MatrixXd &coord_vertex, const std::vector <std::array<int,3>> &surface);
    void save_neighbors_surface_(std::vector<std::array<double,4>> &neighbors_surface_, const std::string& folder, const std::string& filename);
    const int assign_finite_cell_index(Delaunay &T);
    const int assign_all_cell_index(Delaunay &T);
    const std::vector<int> get_index_on_bbox (Eigen::MatrixXd &coord_vertex, Eigen::Matrix<double,2,3> &bbox, Eigen::MatrixXi &index_vertex_per_cell);

    Eigen::MatrixXi get_vertex_index_per_cell(const Delaunay &T);
    Eigen::MatrixXd get_vertex_coord(const Delaunay &T);

    std::pair< std::vector<std::vector<int>>,std::vector<int> > traverse_ray(const Delaunay &T, std::vector<PSL::DepthMap<float,double> > &depthMaps,const int WINDOW_SIZE);
    std::vector < std::array < int, 3> > get_surface(const Delaunay &T, const std::vector<int> &label);
}


#endif // RAYSTRUCTURE_H
