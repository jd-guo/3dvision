#ifndef OPTIMIZER_H
#define OPTIMIZER_H

#include "graph.h"
#include <iostream>
#include <vector>
#include <array>

typedef Graph<int,int,int> GraphType;

namespace GC
{
    const std::vector<std::pair<std::array<int,2>,int>> get_pairwises(const std::vector<std::array<int,4>> &neighbors_, int lambda);

    std::vector<int> get_labels(const std::vector<int> &ray_depths_,
                                const std::vector<std::vector<int> > &ray_traversals_,
                                const std::vector<std::array<int,4>> &neighbors_,
                                const std::vector<std::array<double,4>> &neighbors_surface_,
                                std::vector<double> cooefficient,
                                const int alpha, const int lambda,
                                const int NUM_FINITE_CELLS,
                                const int NUM_ITER,
                                const bool VARIABLE_LAMBDA,
                                const std::vector<int> index_cell_on_bbox,
                                int WINDOW_SIZE);

    void assign_graph_weights(GraphType **g,const int cost, const std::vector<std::vector<int > >& ray_traversals_,
                              int skipped_num, int NUM_FINITE_CELLS, std::vector<int> inclusion,std::vector<double> cooefficient);

    std::vector<int> get_labels_vu(const std::vector<int> &ray_depths_,
                                   const std::vector<std::vector<int> > &ray_traversals_,
                                   const std::vector<std::array<int,4>> &neighbors_,
                                   const std::vector<std::array<double,4>> &neighbors_surface_,
                                   const int alpha, const int lambda,
                                   const int NUM_FINITE_CELLS,
                                   const bool VARIABLE_LAMBDA,
                                   const std::vector<int> index_cell_on_bbox);
}
#endif // OPTIMIZER_H
