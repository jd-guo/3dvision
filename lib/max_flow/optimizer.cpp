#include "optimizer.h"
#include "../ray_structure/setting.h"
#include <ostream>
#include <iostream>
#include <fstream>
#include <vector>
#include <array>

namespace GC {

    const std::vector<std::pair<std::array<int,2>,int>> get_pairwises(const std::vector<std::array<int,4>> &neighbors_, int lambda) {

        std::vector<std::pair<std::array<int,2>,int>> pairwises;

        for (int i=0; i<neighbors_.size(); ++i) {
            for (int j=0; j<4; ++j) {
                int idx_neighbor = neighbors_[i][j];
                if (idx_neighbor!=-1 && i < idx_neighbor) {
                    std::array<int,2> idx_pair = {i,idx_neighbor};
                    pairwises.push_back(make_pair(idx_pair,lambda));
                }
            }
        }
        return pairwises;
    }

    std::vector<int> get_labels(const std::vector<int> &ray_depths_,
                                const std::vector<std::vector<int> > &ray_traversals_,
                                const std::vector<std::array<int,4>> &neighbors_,
                                const std::vector<std::array<double, 4> > &neighbors_surface_,
                                std::vector<double> cooefficient,
                                const int alpha, const int lambda,
                                const int NUM_FINITE_CELLS, const int NUM_ITER,
                                const bool VARIABLE_LAMBDA,
                                const std::vector<int> index_cell_on_bbox,
                                int WINDOW_SIZE) {

        const int NUM_RAYS = ray_depths_.size();
        int NUM_NODES = NUM_FINITE_CELLS + NUM_RAYS*(2*WINDOW_SIZE+1); // Edited for the window approach

        std::vector<int> labels(NUM_FINITE_CELLS);
        std::vector<int> rays;
        for (int i=0; i<NUM_RAYS; ++i) {
            rays.push_back(i);
        }

        // set cost between edges
        std::cout << "Calculating Pairwise cost ..." << std::endl;
        std::vector<std::pair<std::array<int,2>, int> > pairwises;
        if (VARIABLE_LAMBDA) {
            for (int i=0; i<neighbors_.size(); i++){
                for (int j=0; j<4; ++j) {
                    int idx_neighbor = neighbors_[i][j];
                    if (idx_neighbor!=-1  && i < idx_neighbor && idx_neighbor < neighbors_.size()) {
                        std::array<int,2> idx_pair = {i,idx_neighbor};
                        int lambda_temp = (int) (neighbors_surface_[i][j]*LAMBDA_SCALING); // Proper scaling
                        pairwises.push_back(make_pair(idx_pair,lambda_temp));
                        //std::cout << lambda_temp << " ";
                    }
                }
            }
        }
        else {
            pairwises = GC::get_pairwises(neighbors_,lambda);
        }
        std::cout << "Done" << std::endl << std::endl;


        int WINDOW_LENGTH = 2*WINDOW_SIZE+1;
        std::vector<int> inclusion(NUM_RAYS,(1<<(WINDOW_LENGTH+1))-1); //  take all possible rays into consideration,0b11...1
              // iterate Graph Cut
        int Valid_ray_num = WINDOW_LENGTH* NUM_RAYS;
        for (int iter=0; iter<NUM_ITER; ++iter) {

            std::cout << "/// " << iter << "th iteration of Graph Cut ///" << std::endl;
            std::cout << "Number of valid rays: " << Valid_ray_num << std::endl;
            std::cout << "Currently using coefficients: ";
            for (int id = 0; id < cooefficient.size(); id++) std::cout << cooefficient[id] << " ";
            GraphType * g = new GraphType(NUM_NODES, pairwises.size());
            g->add_node(NUM_NODES);
            std::cout << "\nNumber of Nodes: " << g->get_node_num() << std::endl;

            // Currently window approach with cooefficient along the ray
            for (int wind_idx = 0; wind_idx < WINDOW_LENGTH; wind_idx++){
              assign_graph_weights(&g,cooefficient[wind_idx]*alpha,ray_traversals_,wind_idx,NUM_FINITE_CELLS,inclusion,cooefficient); //
            }


            // construct graph


            // connect neighbors
            for (int i=0; i<pairwises.size(); ++i)
                g->add_edge(pairwises[i].first[0], pairwises[i].first[1], pairwises[i].second, pairwises[i].second);

            for (int i=0; i<index_cell_on_bbox.size(); ++i) {
                int idx = index_cell_on_bbox[i];
                g->add_tweights(idx, 100000, 0);
            }

            // optimize
            std::cout << "Maxflowing ..." << std::endl;
            double flow = g->maxflow();
            std::cout << "the flow amounts to ..." << flow << std::endl;
            std::cout << "Setting labels ..." << std::endl;
            int sink_count =0;
            int source_count = 0;
            for (int i=0; i<NUM_FINITE_CELLS; ++i) {
              //std::cout << "the cell number " << i << std::endl;
                if (g->what_segment(i) == GraphType::SOURCE){
                    labels[i] = 0;
                    source_count++;
                  }
                else{
                    labels[i] = 1;
                    sink_count++;
                  }
            }
            std::cout << " num of source node: " << source_count << "num of sink node: " << sink_count<< std::endl;
            //delete g;
            g = NULL;

            std::cout << "Choosing valid rays ..." << std::endl;
            std::vector<int> inclusion_nxt = inclusion; // Copy of last inclusion;
            int exclude_counter = 0;
            Valid_ray_num = 0;
            for (int idx=0; idx<NUM_RAYS; ++idx) {
                int length = ray_traversals_[idx].size();
                if (length < (2*WINDOW_SIZE+1)) continue;
                for (int skip= 0; skip <(2*WINDOW_SIZE+1) ; skip++){
                  if ((inclusion_nxt[idx] & (1<<skip) ) && (labels[ray_traversals_[idx][length-1-skip]]==0)){
                  // if originally included and now set to SOURCE
                    exclude_counter++;
                    inclusion_nxt[idx] = (inclusion_nxt[idx] & ~(1<<skip));
                  }
                }
                Valid_ray_num += ((inclusion_nxt[idx] & 1)+ (inclusion_nxt[idx] & 2)/2+
                                  (inclusion_nxt[idx] & 4)/4+(inclusion_nxt[idx] & 8)/8+
                                  (inclusion_nxt[idx] & 16)/16);
                //if (idx_cell_depths!=-1 && labels[idx_cell_depths]==1)
                //     rays_.push_back(idx_ray);
             }
            //
             if (exclude_counter!=0) { // Change happened
                inclusion = inclusion_nxt;
                 std::cout << "Done, emitted number of rays in this iteration: " << exclude_counter << std::endl;
             }
            //
            else {
                std::cout << "Done, no more iteration required" << std::endl;
                 break;
           }
        }
        return labels;
    }

    void assign_graph_weights(GraphType **g,
                              const int cost,
                              const  std::vector<std::vector<int > >& ray_traversals_,
                              int skipped_num,
                              int NUM_FINITE_CELLS,
                              std::vector<int> inclusion,
                              std::vector<double> cooefficient){
      int window_length = cooefficient.size();
      //std::cout << window_length;
      for (int i=0; i<ray_traversals_.size(); ++i) {
          if (ray_traversals_[i].size()<window_length) continue; // Too short rays
          //int idx_ray = rays[i];
          if (inclusion[i] & (1<< skipped_num)){  // if this ray to be included
            int curr_node = NUM_FINITE_CELLS+window_length*i+skipped_num;

            (*g)->add_tweights(curr_node,cost,0); // Original rays
            //std::cout << "The " << i << "th Ray: " << "Added Source from current\n";
            // connect cells outside of the surface
            for (int j=0; j<ray_traversals_[i].size()-skipped_num-1; ++j) {

                //std::cout << j << "th item of the ray structure with size" << ray_traversals_[i].size()<< ".\n";
                int idx = ray_traversals_[i][j];
                //std::cout << "traversal towards" << idx << " works\n";
                if ((idx != -1 ) && (idx < NUM_FINITE_CELLS)) {
                    (*g)->add_edge(curr_node,idx,cost,0);
                }
            }

            //std::cout << "traversal works\n";

            // connect cells inside of the surface
            int last_idx = ray_traversals_[i].size()-skipped_num-1;
            (*g)->add_tweights(ray_traversals_[i][last_idx],0,cost);
            //std::cout << "leak works\n";
        }
      }
    }


    std::vector<int> get_labels_vu(const std::vector<int> &ray_depths_,
                                   const std::vector<std::vector<int> > &ray_traversals_,
                                   const std::vector<std::array<int,4>> &neighbors_,
                                   const std::vector<std::array<double,4>> &neighbors_surface_,
                                   const int alpha, const int lambda,
                                   const int NUM_FINITE_CELLS,
                                   const bool VARIABLE_LAMBDA,
                                   const std::vector<int> index_cell_on_bbox) {

        //const int NUM_RAYS = ray_depths_.size();
        int NUM_NODES = NUM_FINITE_CELLS; // + NUM_RAYS;

        std::vector<int> labels(NUM_FINITE_CELLS);
//        std::vector<int> rays;
//        for (int i=0; i<NUM_RAYS; ++i) {
//            rays.push_back(i);
//        }


        // initialize unaries
//        std::vector<std::array<int,2>> unaries;
//        for (int i=0; i<NUM_NODES; ++i) {
//            std::array<int,2> init = {0,0};
//            unaries.push_back(init);
//        }
/*
        // assign unaries
        for (int i=0; i<ray_depths_.size(); ++i) {
            int idx = ray_depths_[i];
            if (idx != -1) {
                unaries[idx][1] += alpha;
            }
        }

        for (int i=0; i<ray_traversals_.size(); ++i) {
            if (ray_traversals_[i].size()!=0){
                    int idx = ray_traversals_[i][0];
                    unaries[idx][0] += alpha;
            }
        }
*/

        // get pairwise
  /*      std::vector<std::array<int,4> >pairwises;

        // assign tranverse pairwises
        for (int i=0; i<ray_depths_.size(); ++i) {
            if (ray_traversals_[i].size()!=0){
                for (int j=0; j<ray_traversals_[i].size()-1; ++j) {
                    int idx = ray_traversals_[i][j];
                    int idx_next = ray_traversals_[i][j+1];
                    std::array<int,4> idx_pair = {idx,idx_next,alpha,0};
                    std::cout << pairwises.size() << std::endl;
                    pairwises.push_back(idx_pair);
                }
            }

        }
*/
/*        for (int i=0; i<neighbors_.size(); ++i) {
            for (int j=0; j<4; ++j) {
                int idx_neighbor = neighbors_[i][j];
                if (idx_neighbor!=-1 && i < idx_neighbor && idx_neighbor < neighbors_.size()) {
                    std::array<int,4> idx_pair ;
                    if (VARIABLE_LAMBDA){
                        int lambda_temp = (int) (neighbors_surface_[i][j]*100000000);
                        idx_pair = {i,idx_neighbor,lambda_temp,lambda_temp};
                    }
                    else {
                        idx_pair = {i,idx_neighbor,lambda,lambda};
                    }
                    pairwises.push_back(idx_pair);
                }
            }
        }
*/
        // set cost between edges
        std::cout << "Calculating Pairwise cost ..." << std::endl;
        std::vector<std::pair<std::array<int,2>, int> > pairwises;
        if (VARIABLE_LAMBDA) {
            for (int i=0; i<neighbors_.size(); i++){
                for (int j=0; j<4; ++j) {
                    int idx_neighbor = neighbors_[i][j];
                    if (idx_neighbor!=-1  && i < idx_neighbor && idx_neighbor < neighbors_.size()) {
                        std::array<int,2> idx_pair = {i,idx_neighbor};
                        int lambda_temp = (int) (neighbors_surface_[i][j]*100000); //1000000);
                        // scale the lambda into something between 1 and 10000
                        pairwises.push_back(make_pair(idx_pair,lambda_temp));
                    }
                }
            }
        }
        else {
            pairwises = GC::get_pairwises(neighbors_,lambda);
        }

        GraphType *g = new GraphType(NUM_NODES, NUM_NODES*10); //pairwises.size()+unaries.size());
        g->add_node(NUM_NODES);
//        for (int i=0; i<pairwises.size(); ++i){
//            g->add_edge(pairwises[i][0],pairwises[i][1], pairwises[i][2], pairwises[i][3]);
//        }

        // assign unaries
        //for (int i=0; i<unaries.size(); ++i) {
        //    g->add_tweights(i,unaries[i][0],unaries[i][1]);
        //}

        for (int i=0; i<ray_traversals_.size(); ++i) {
            if (ray_traversals_[i].size() != 0) {
                for (int j=0; j<ray_traversals_[i].size()-1; ++j) {
                    if (j==0) {
                        //std::cout << "i: " << i << " j: " << j << std::endl;
                        int idx = ray_traversals_[i][j];
                        //std::cout << "idx: " << idx << std::endl;
                        if (idx != -1) {
                            g -> add_tweights(idx,alpha,0);
                        }
                    }
                    else {
                        int idx_prev = ray_traversals_[i][j-1];
                        int idx_curr = ray_traversals_[i][j];
                        if (idx_curr != -1) {
                            g -> add_edge(idx_prev,idx_curr,alpha,0);
                        }
                    }
                }
            }
        }

        for (int i=0; i<ray_depths_.size(); ++i) {
            int idx = ray_depths_[i];
            if (idx!=-1) {
                g -> add_tweights(idx,0,alpha);
            }
        }

        // connect neighbors
        for (int i=0; i<pairwises.size(); ++i)
            g -> add_edge(pairwises[i].first[0], pairwises[i].first[1], pairwises[i].second, pairwises[i].second);

        for (int i=0; i<index_cell_on_bbox.size(); ++i) {
            int idx = index_cell_on_bbox[i];
            g -> add_tweights(idx,1000,0);
        }


        // optimize
        std::cout << "Maxflowing ..." << std::endl;
        double flow = g->maxflow();

        std::cout << "Setting labels ..." << std::endl;
        for (int i=0; i<NUM_FINITE_CELLS; ++i) {
            if (g->what_segment(i) == GraphType::SOURCE)
                labels[i] = 0;
            else
                labels[i] = 1;
        }

        delete g;
        g = NULL;
        return labels;
    }

}
